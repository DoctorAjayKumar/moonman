#!/usr/bin/env escript

% checking that my function to specify the board is correct

-mode(compile).

main([]) ->
    board_printchar({rc, 0, 0}).


% end of the board
board_printchar(RC = {rc, 30, 27}) ->
    SQT = mm_squares:square_type(RC),
    ok = print_sqchar(SQT),
    ok = newline(),
    ok;
% end of the row, print newline, increment the row and reset the column
board_printchar(RC = {rc, R, 27}) ->
    SQT = mm_squares:square_type(RC),
    ok = print_sqchar(SQT),
    ok = newline(),
    board_printchar({rc, R + 1, 0});
% not the end of the row, increment the column and keep the row
% constant, and don't print a newline
board_printchar(RC = {rc, R, C}) ->
    SQT = mm_squares:square_type(RC),
    ok = print_sqchar(SQT),
    board_printchar({rc, R, C + 1}).


print_sqchar(interior) ->
    io:format(" ");
print_sqchar(boundary) ->
    io:format("X").


newline() ->
    io:format("~n").
