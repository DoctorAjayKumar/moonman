% @doc
% module for querying information about squares
-module(mm_squares).

-export_type([
    rc/0,
    square_type/0
]).
-export([
    square_type/1
]).

-type idx0()        :: non_neg_integer().
-type rc()          :: {rc, idx0(), idx0()}.

% Forgetting about that weird half boundary for now in the middle
-type square_type() :: interior | boundary.

%%% API

-spec square_type(rc()) -> square_type().
% @doc
% Returns the type of square
%
% Will beat the row coordinate down mod 28, the column coordinate
% down mod 31

square_type({rc, R, C}) ->
    sqt({rc, modulo(R, nr()), modulo(C, nc())}).


%%% internals

-spec modulo(Integer, Modulo) -> Result
        when Integer :: integer(),
             Modulo  :: pos_integer(),
             Result  :: non_neg_integer().
% @private modulo function that behaves the way you expect; i.e. X
% mod N always returns a result between 0..N-1

modulo(X, Mod) when is_integer(X), is_integer(Mod), 2 =< Mod ->
    modulo_(X, Mod).




modulo_(X, Mod) when 0 =< X ->
    X rem Mod;
modulo_(X, Mod) when X < 0 ->
    Mod + (X rem Mod).



nc() -> 28.



nr() -> 31.



-spec sqt({rc, 0..27, 0..30}) -> interior | boundary.
% @private
% Works by listing all of the interior points and everything that is
% not an interior point is a boundary point
%
% Note to self: next time go by columns; there's left-right
% reflectional symmetry, but not up-down reflectional symmetry
% @end

% row 1 is mostly interior save for points 13 and 14
sqt({rc, 1, C}) when 1 =< C, C =< 12 -> interior;
sqt({rc, 1, C}) when 15 =< C, C =< 26 -> interior;
% row 2 only has a small number of interior points
sqt({rc, 2, 1}) -> interior;
sqt({rc, 2, 6}) -> interior;
sqt({rc, 2, 12}) -> interior;
sqt({rc, 2, 15}) -> interior;
sqt({rc, 2, 21}) -> interior;
sqt({rc, 2, 26}) -> interior;
% row 3 is the same as row 2
sqt({rc, 3, 1}) -> interior;
sqt({rc, 3, 6}) -> interior;
sqt({rc, 3, 12}) -> interior;
sqt({rc, 3, 15}) -> interior;
sqt({rc, 3, 21}) -> interior;
sqt({rc, 3, 26}) -> interior;
% row 4 is the same as row 2
sqt({rc, 4, 1}) -> interior;
sqt({rc, 4, 6}) -> interior;
sqt({rc, 4, 12}) -> interior;
sqt({rc, 4, 15}) -> interior;
sqt({rc, 4, 21}) -> interior;
sqt({rc, 4, 26}) -> interior;
% row 5 is all interior points except the boundaries
sqt({rc, 5, C}) when 1 =< C, C =< 26 -> interior;
% row 6 is mostly boundary
sqt({rc, 6, 1}) -> interior;
sqt({rc, 6, 6}) -> interior;
sqt({rc, 6, 9}) -> interior;
sqt({rc, 6, 18}) -> interior;
sqt({rc, 6, 21}) -> interior;
sqt({rc, 6, 26}) -> interior;
% row 7 is the same as row 6
sqt({rc, 7, 1}) -> interior;
sqt({rc, 7, 6}) -> interior;
sqt({rc, 7, 9}) -> interior;
sqt({rc, 7, 18}) -> interior;
sqt({rc, 7, 21}) -> interior;
sqt({rc, 7, 26}) -> interior;
% row 8 is mostly interior points
sqt({rc, 8, C}) when 1 =< C, C =< 6 -> interior;
sqt({rc, 8, C}) when 9 =< C, C =< 12 -> interior;
sqt({rc, 8, C}) when 15 =< C, C =< 18 -> interior;
sqt({rc, 8, C}) when 21 =< C, C =< 26 -> interior;
% row 9 is mostly boundary points
sqt({rc, 9, 6}) -> interior;
sqt({rc, 9, 12}) -> interior;
sqt({rc, 9, 15}) -> interior;
sqt({rc, 9, 21}) -> interior;
% row 10 looks to be identical to row 9 but don't quote me on that
sqt({rc, 10, 6}) -> interior;
sqt({rc, 10, 12}) -> interior;
sqt({rc, 10, 15}) -> interior;
sqt({rc, 10, 21}) -> interior;
% row 11 is similar to rows 9 and 10, but has a free range in the
% middle
sqt({rc, 11, 6}) -> interior;
sqt({rc, 11, C}) when 9 =< C, C =< 18 -> interior;
sqt({rc, 11, 21}) -> interior;
% row 12 is mostly boundary
sqt({rc, 12, 6}) -> interior;
sqt({rc, 12, 9}) -> interior;
sqt({rc, 12, 18}) -> interior;
sqt({rc, 12, 21}) -> interior;
% row 13 is same as row 12
sqt({rc, 13, 6}) -> interior;
sqt({rc, 13, 9}) -> interior;
sqt({rc, 13, 18}) -> interior;
sqt({rc, 13, 21}) -> interior;
% row 14 is the most interesting row, because it's the only one where
% the toroidal structure comes into play
sqt({rc, 14, C}) when 0 =< C, C =< 9 -> interior;
sqt({rc, 14, C}) when 18 =< C, C =< 27 -> interior;
% row 15 is mostly boundary
sqt({rc, 15, 6}) -> interior;
sqt({rc, 15, 9}) -> interior;
sqt({rc, 15, 18}) -> interior;
sqt({rc, 15, 21}) -> interior;
% row 16 is mostly boundary
sqt({rc, 16, 6}) -> interior;
sqt({rc, 16, 9}) -> interior;
sqt({rc, 16, 18}) -> interior;
sqt({rc, 16, 21}) -> interior;
% row 17 has an interval in the middle
sqt({rc, 17, 6}) -> interior;
sqt({rc, 17, C}) when 9 =< C, C =< 18 -> interior;
sqt({rc, 17, 21}) -> interior;
% rows 18 and 19 are the same as 16
sqt({rc, 18, 6}) -> interior;
sqt({rc, 18, 9}) -> interior;
sqt({rc, 18, 18}) -> interior;
sqt({rc, 18, 21}) -> interior;
sqt({rc, 19, 6}) -> interior;
sqt({rc, 19, 9}) -> interior;
sqt({rc, 19, 18}) -> interior;
sqt({rc, 19, 21}) -> interior;
% row 20 is mostly interior
sqt({rc, 20, C}) when 1 =< C, C =< 12 -> interior;
sqt({rc, 20, C}) when 15 =< C, C =< 26 -> interior;
% Rows 21 and 22 are the same, mostly boundary
sqt({rc, 21, 1}) -> interior;
sqt({rc, 21, 6}) -> interior;
sqt({rc, 21, 12}) -> interior;
sqt({rc, 21, 15}) -> interior;
sqt({rc, 21, 21}) -> interior;
sqt({rc, 21, 26}) -> interior;
sqt({rc, 22, 1}) -> interior;
sqt({rc, 22, 6}) -> interior;
sqt({rc, 22, 12}) -> interior;
sqt({rc, 22, 15}) -> interior;
sqt({rc, 22, 21}) -> interior;
sqt({rc, 22, 26}) -> interior;
% row 23 is mostly interior
sqt({rc, 23, C}) when 1 =< C, C =< 3 -> interior;
sqt({rc, 23, C}) when 6 =< C, C =< 21 -> interior;
sqt({rc, 23, C}) when 24 =< C, C =< 26 -> interior;
% row 24 is mostly boundary, and is the same as row 25
sqt({rc, 24, 3}) -> interior;
sqt({rc, 24, 6}) -> interior;
sqt({rc, 24, 9}) -> interior;
sqt({rc, 24, 18}) -> interior;
sqt({rc, 24, 21}) -> interior;
sqt({rc, 24, 24}) -> interior;
sqt({rc, 25, 3}) -> interior;
sqt({rc, 25, 6}) -> interior;
sqt({rc, 25, 9}) -> interior;
sqt({rc, 25, 18}) -> interior;
sqt({rc, 25, 21}) -> interior;
sqt({rc, 25, 24}) -> interior;
% row 26 is mostly interior
sqt({rc, 26, C}) when 1 =< C, C =< 6 -> interior;
sqt({rc, 26, C}) when 9 =< C, C =< 12 -> interior;
sqt({rc, 26, C}) when 15 =< C, C =< 18 -> interior;
sqt({rc, 26, C}) when 21 =< C, C =< 26 -> interior;
% rows 27 and 28 are the same and are mostly boundary
sqt({rc, 27, 1}) -> interior;
sqt({rc, 27, 12}) -> interior;
sqt({rc, 27, 15}) -> interior;
sqt({rc, 27, 26}) -> interior;
sqt({rc, 28, 1}) -> interior;
sqt({rc, 28, 12}) -> interior;
sqt({rc, 28, 15}) -> interior;
sqt({rc, 28, 26}) -> interior;
% row 29 is all interior except for 0 and 27
sqt({rc, 29, C}) when 1 =< C, C =< 26 -> interior;
% everything not above is a boundary
sqt({rc, _, _}) -> boundary.
